package test;

import java.io.IOException;

import org.apache.hadoop.HadoopIllegalArgumentException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.RPC;
import org.apache.hadoop.ipc.Server;

/**
 * <strong>RPC</strong>
 * remote procedure call 远程调用协议，不同进程之间的方法调用,通过网络通信<br>
 * 在hadoop中，client和hdfs之间的调用，namenode和datanode之间的通信都是RPC协议<br>
 * <strong>eg</strong>:webService跨语言跨系统的调用接口解决方案<br>
 * <strong>CRM</strong>
 * customer relationship management
 * 客户关系管理系统
 * @author smj
 *
 */
public class RPCDemo implements Bizable{

    public void sayHi(String name){
        
        System.out.println("hello "+name);
    }
    
    public static void main(String[] args) throws HadoopIllegalArgumentException, IOException {
        
        Configuration conf = new Configuration();
        
        //rpc配置启动一个远程的服务
        Server server = new RPC.Builder(conf).setProtocol(Bizable.class).setInstance(new RPCDemo()).setBindAddress("192.168.1.202").setPort(8000).build();
    
        server.start();
    }
    
}
