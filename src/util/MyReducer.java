package util;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class MyReducer extends Reducer<Text, LongWritable, Text, LongWritable>{

    @Override
    protected void reduce(Text keyIn, Iterable<LongWritable> valueIns,
            Reducer<Text, LongWritable, Text, LongWritable>.Context context) throws IOException, InterruptedException {
        
       /* super.reduce(keyIn, valueIns, context);*/
        //接受数据
        
        //定义一个计数器
        long counter = 0;
        for (LongWritable lw : valueIns) {
            
            counter+=lw.get();
        }
        context.write(keyIn, new LongWritable(counter));
    }

    
}
