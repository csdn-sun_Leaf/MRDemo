package test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import util.MyMapper;
import util.MyReducer;

/**
 * 这个类只能由hadoop来执行，因为地址是hadoop的地址，不由hadoop执行找不到路径
 * 1、根据业务逻辑需求，设定输入和输出值
 * 2、创建类分别继承Mapper和Reducer,重写map和reduce方法，处理逻辑，将新的key，value输出到文件系统
 * 3、将自定义的mapper和reducer通过job对象组装起来写在处理类中，打包上传到服务器
 * 
 * 这里的关键在于理解数据在mapper和reducer之间的流转过程和处理结果
 * @author sunmj
 *
 */
public class Wc {

    public static void main(String[] args) throws IOException, URISyntaxException, ClassNotFoundException, InterruptedException {
        
        Job job = Job.getInstance(new Configuration());
        //将main方法所在的类加进去
        job.setJarByClass(Wc.class);
        
        //设置mapper相关的属性
        job.setMapperClass(MyMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(LongWritable.class);
        
        FileInputFormat.setInputPaths(job, new Path(new URI("/word/words.txt")));
        
        //设置reducer相关的属性
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(LongWritable.class);
        job.setReducerClass(MyReducer.class);
        
        FileOutputFormat.setOutputPath(job, new Path(new URI("/wcout1")));
        
        job.waitForCompletion(true);//提交并等待执行完成，执行中打印执行详情
        
    }
}
