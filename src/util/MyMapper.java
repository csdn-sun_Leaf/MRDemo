package util;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class MyMapper extends Mapper<LongWritable, Text, Text, LongWritable>{

    /* 重写map方法
     * @see org.apache.hadoop.mapreduce.Mapper#map(KEYIN, VALUEIN, org.apache.hadoop.mapreduce.Mapper.Context)
     */
    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, LongWritable>.Context context)
            throws IOException, InterruptedException {
        
        /*super.map(key, value, context);
         * */
        
        //接受数据为1
        String line = value.toString();
        
        //切分数据
        String[] words = line.split(" ");
        
        for (String word : words) {
            
            //单词出现一次，记一个1，输出
            context.write(new Text(word), new LongWritable(1));
        }
    }

    
    
}
