package test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;

public class HdfsTest {

    public static void main(String[] args) throws URISyntaxException, IOException {
        
        //这是一个抽象类，不能new
        FileSystem fs = null;
        
        URI uri = new URI("hdfs://master:9000");
        
        fs = FileSystem.get(uri, new Configuration());
       
        FSDataInputStream in = fs.open(new Path("/jdk.tar.gz"));
        
        
        OutputStream out = new FileOutputStream(new String("G:/jdk.tar.gz"));
        
        IOUtils.copyBytes(in, out, 4096,true);
        /*FileStatus[] fss = fs.listStatus(new Path("/"));
        for (FileStatus file : fss) {
            
            System.out.println(file.getOwner());
        }*/
    }
}
