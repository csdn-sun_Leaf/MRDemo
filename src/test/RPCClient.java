package test;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.RPC;

public class RPCClient {

    public static void main(String[] args) throws IOException {
        
        InetSocketAddress addr = new InetSocketAddress("192.168.1.202", 8000);
        //hadoop2.0要求接口里面要有一个versionID字段,代理对象一般是$开头
        Bizable proxy = RPC.getProxy(Bizable.class, 10010, addr, new Configuration());
        
        //问题：versionID参数不对也可以调用
        proxy.sayHi("123");
        
        //调用代理对象的方法，执行方法的是被代理的对象，所以此时返回参数的是服务器端的
        
        //这里的sayHi方法调用的其他进程中的方法
        
        RPC.stopProxy(proxy);
    }
}
