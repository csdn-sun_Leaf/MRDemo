package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.commons.compress.utils.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.Before;
import org.junit.Test;

public class JTest {

    /**
     *hadoop下的所有的fs操作都会在这里找到相应的操作 
     */
    FileSystem fs = null;
    @Before
    public void init() throws IOException, URISyntaxException, InterruptedException{
        
        //首先创建一个filesystem的实现类（工具类）
        fs = FileSystem.get(new URI("hdfs://master:9000"), new Configuration(),"root");
    }
    
    /**
     * 上传文件
     * @throws IOException 
     * @throws IllegalArgumentException 
     * @throws NumberFormatException 
     */
    @Test
    public void upload() throws NumberFormatException, IllegalArgumentException, IOException{
        
        //读取本地文件
        InputStream in = new FileInputStream(new File("G:/zhsx1.sql"));
        
        /*会有权限问题，当前用户没有写文件的权限,需要给filesystem指定用户
         * 
         * 这个方式导致hadoop不安全的问题，另有项目完成安全保障
         * */
        OutputStream out = fs.create(new Path("/zhsx/zhsx1.sql"), true, 4096, new Short("2"), 128*1024*1024);
        
        IOUtils.copy(in, out, 4096);
    }
    
    /**
     * 文件下载demo，将文件copy到本地
     * @throws IllegalArgumentException
     * @throws IOException
     */
    @Test
    public void download() throws IllegalArgumentException, IOException{
        
        /*参数说明：是否删除源文件，源文件路径，目标路径，是否允许本地文件系统管理（设为false会报错）*/
        fs.copyToLocalFile(false, new Path("/zhsx/zhsx1.sql"), new Path("G:/mysql.sql"), true);
    }
    
    /**
     * 删除文件 
     * @throws IllegalArgumentException
     * @throws IOException
     */
    @Test
    public void delete() throws IllegalArgumentException, IOException{
        
        /*参数说明：路径，是否递归删除*/
        fs.delete(new Path("/zhsx"), true);
    }
}
